#!/usr/bin/perl -w
# 0 = heads, 1 = tails
my ($tails, $flips) = 0;
for ($flips = 0; $flips < 1_000_000; $flips++)
{
    my @result = (int(rand(2)), int(rand(2)));
    $tails++ if ($result[0] or $result[1])
}
printf "Final result: %.8f\n", 100*($tails / $flips);
