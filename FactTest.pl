#!/usr/bin/perl
use 5.018;
use strict;
use Benchmark qw(timethese);

my $count = shift or die;

timethese
(
    $count,
    {
        'Iterative Factorial one' => &fac1,
        'Iterative Factorial two' => &fac2,
#       'Recursive Factorial one' => &fac3,
    }
);

sub fac1
{
    my ($f, $n) = (1, 20);
    $f *= $_ for (2 .. $n)
    $f;
}

sub fac2
{
    my ($f, $n) = (1, 20);
    map { $f *= $_ } (2 .. $n);
    $f;
}

sub fac3
{
    no warnings 'recursion';
    my $n = shift or 10;
    my $f = shift or 1;
    return $f if ($n <= 1);
    @_ = $n - 1, $f * $n;
    goto &fac3;
}
