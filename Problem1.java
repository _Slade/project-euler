import java.util.*;

public class Problem1
{
    public static void main (String[] args)
    {
        HashSet<Integer> list = new HashSet<Integer>();
        for (int i = 0; i < 1000; i+=3)
        {
            System.out.print(i + " ");
            list.add(i);
        }
        for (int i = 0; i < 1000; i+=5)
        {
            System.out.print(i + " ");
            list.add(i);
        }
        int sum = 0;
        for (Integer i : list)
        {
            sum += i;
        }
        System.out.println(sum);
    }
}
