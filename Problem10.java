import java.util.*;
import java.math.*;

public class Problem10
{
    public static void main (String[] args)
    {
        long sumOfPrimes = 1;
        for (int i = 1; i < 2000000; i += 2)
        {
            if (isPrime(i))
            {
                sumOfPrimes += i;
            }
        }
        System.out.println(sumOfPrimes);
    }
    public static boolean isPrime (int n)
    {
        if (n % 2 == 0)
            return false;
        for (int d = 3 ; d <= Math.sqrt(n); d += 2)
        {
            if (n % d == 0)
                return false;
        }
        return true;
    }
}
