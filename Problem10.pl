#!/usr/bin/perl
print problem10(), "\n";
sub problem10
{ # Finds the sum of all primes < 2,000,000
    for ($a = 1 ; $a < 2e6 ; $a += 2)
    {
        for ($d = 3 ; $d <= sqrt($a) ; $d += 2)
        {
            if ($a % $d == 0 || $a % 2 == 0) { $total -= $a; last; }
        }
        $total += $a;
    }
    return ++$total;
}

