public class Problem12
{
    public static void main (String[] args)
    {
        for (int i = 0;; i++)
        {
            double a = triNum(i);
            double b = numDiv(a);
            System.out.printf("\u03C3(T(%.0f))\t= %3.0f\n", a, b);
            if (b >= 500)
                break;
        }
    }
    public static double triNum (double n)
    {
        return (n*(n+1))/2;
    }
    public static double numDiv (double n)
    {
        int numOfDivisors = 0;
        for (int i = 1; i < Math.sqrt(n); i++)
            if (n % i == 0)
                numOfDivisors++;
        return numOfDivisors * 2;
    }
}
