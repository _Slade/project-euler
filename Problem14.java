/*
 * For the first 10^6 positive whole numbers, which recurses the most times
 * before it reaches 1 in the Collatz sequence?
 */
public class Problem14
{
    public static void main (String[] args)
    {
        double greatestIter = 0;
        double winner = 1;
        for (int i = 1; i < 1_000_000; i++)
        {
            // Use temp var to avoid recomputing sequence
            double currentIter = collatz(i, 0);
            if (currentIter > greatestIter)
            {
                greatestIter = currentIter;
                winner       = i;
            }
        }
        System.out.printf("%.0f\n", winner);
    }
    public static double collatz (double n, double numIter)
    {
        if (n == 1)
            return numIter + 1;
        if (n % 2 == 0)
            return collatz(n / 2, numIter + 1);
        else
            return collatz(3*n+1, numIter + 1);
    }
}
