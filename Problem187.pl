#!/usr/bin/perl
use 5.018;
use warnings;
use integer;
use Math::Prime::Util qw(forprimes prime_count);
my $limit = shift || 1e8-1;
my ($sum, $pc) = (0, 1);
forprimes
{
    $sum += prime_count($limit / $_) + 1 - $pc++;
} sqrt($limit);
say $sum;
