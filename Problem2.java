// The trick to this is that the fibonacci sequence follows the pattern odd odd even
import java.util.*;

public class Problem2
{
    public static void main (String[] args)
    {
        int sum = 0;
        LinkedList<Integer> fiblist = new LinkedList<Integer>();
        for (int i = 0; i < 32; i++)
        {
            int j = fib(i);
            if (j % 2 == 0)
                fiblist.add(j);
        }
        for (Integer i : fiblist)
            sum += i;
        System.out.println(sum);
    }
    public static int fib (int n)
    {
        if (n == 0)
            return 0;
        if (n == 1)
            return 1;
        return fib(n - 1) + fib(n - 2);
    }
}
