#!/usr/bin/perl
use 5.020;
use warnings;
use constant PHI => (1 + sqrt(5))/2;

sub fib { int((PHI**$_[0] / sqrt(5)) + 0.5) }
my ($sum, $n) = (0, 1);
for (; (my $F = fib($n)) < 4_000_000; ++$n)
{
    $sum += $F if !($F & 1);
}
print $sum, "\n";
