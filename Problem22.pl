#!/usr/bin/perl
use 5.018;
use warnings;
use List::MoreUtils qw(zip);
open (my $name_file, "<", "names.txt")
    or die "Couldn't open file for reading: $!\n";
my %alphabet = map { state $index = 0; $_ => $index++ } ("\n", "A" .. "Z");
my $result = 0;
while (my $line = <$name_file>)
{
    my @chars = split('', $line);
    my $char_vals = 0;
    foreach (@chars) { $char_vals += $alphabet{"$_"}; }
    $result += $char_vals * $.;
}
say $result;
close($name_file)
    or warn "Close failed: $!\n";
