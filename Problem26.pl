#!/usr/bin/perl
use 5.018;
use warnings;
use bignum;
use Math::Prime::Util qw<primes>;
my @primes = @{primes(1000)};
for my $x (@primes)
{
    for (2 .. 1001)
    {
        say $x if (10**$x % $_ == 1);
    }
}
