#!/usr/bin/perl
use 5.018;
use warnings;
use POSIX;
$x = int floor((shift @ARGV)/2);
printf "%d\n", (16 / 3) * $x**3 + 10 * $x**2 + (26 / 3) * $x + 1;
