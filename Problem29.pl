#!/usr/bin/perl
use 5.018;
use warnings;
use bignum;

my %set;
for my $a (2 .. 100)
{
    for my $b (2 .. 100)
    {
        $set{$a**$b} = 1;
    }
}
print scalar keys %set;
