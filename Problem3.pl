#!/usr/bin/perl
use 5.018;
use warnings;
for $a (reverse 100 .. 999)
{
    for $b (reverse 100 .. $a)
    {
        if ($a * $b == reverse $a * $b)
        {
            $max = $a * $b if ($a * $b > $max);
        }
    }
}
print "$max\n";
