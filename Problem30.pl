#!/usr/bin/perl -w
use 5.018;
use strict;
use List::Util qw(sum);

my %fpod_nums;
for (2..999999)
{
    $fpod_nums{$_} = 1 if ($_ == fpod($_));
}
print join ' ', keys %fpod_nums;
printf "\nSum: %d\n", sum(keys %fpod_nums);

sub fpod { sum(map { $_ ** 5 } split '', "$_[0]") }
