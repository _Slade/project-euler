#!/usr/bin/perl
use 5.018;
use warnings;
my @n = (2, 3, 5, 7, 11, 13, 17, 37, 79, 113, 197, 199, 337, 1193, 3779, 11939, 19937, 193939, 199933);
my %f;
map { map { $f{$_} = 1 } rotate($_) } @n;
print scalar keys %f;
sub rotate
{
    local $" = '';
    my @n = split '', shift @_;
    my @l;
    push @l, "@n";
    for (1 .. $#n)
    {
        push @n, shift @n;
        push @l, "@n";
    }
    @l;
}
