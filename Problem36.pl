#!/usr/bin/perl -w
use 5.018;
use strict;
use List::Util qw(sum);

my @dbp = grep { is_palindrome($_) && is_palindrome(dec2bin($_)) } 1 .. 1e6;
print join ' ', @dbp;
printf "\n%d\n", sum(@dbp);

sub dec2bin
{
    my $bin = unpack("B32", pack("N", shift));
    $bin =~ s/^0+(?=\d)//r;
}

sub is_palindrome
{
    my $n = shift;
  # n >> 1 == ⌊n / 2⌋ for any integer n
    for my $i (0 .. length($n)>>1)
    {
        return 0 if (substr("$n", $i, 1) ne substr("$n", -$i-1, 1));
    }
    1;
}
