#!/usr/bin/perl
use 5.018;
use warnings;
my @n;
for (my $i = 1; @n < 1000000; ++$i)
{
    push (@n, split('', $i));
}
my $sol = 1;
for (my $i = 1; $i < 10000000; $i*=10)
{
    $sol *= $n[$i-1];
}
printf "%d\n", $sol;
