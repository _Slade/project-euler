#!/usr/bin/perl
use 5.018;
use warnings;
use List::Util qw<sum>;

open(my $fh, "<", "words.txt") or die "Couldn't open: $!\n";
my %trinums  = map  { (($_*($_+1))/2) => 1  } (1 .. 18);
my @triwords = grep { $trinums{wordval($_)} } <$fh>;
close $fh;
print  "@triwords\n";
printf "%d words are triangle words.\n", scalar @triwords;
sub wordval
{
    chomp;
    my @lettervals = map { ord($_) - 64 } split '', uc shift;
    sum(@lettervals);
}
