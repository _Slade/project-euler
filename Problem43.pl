#!/usr/bin/perl -w
use 5.018;
use strict;
use List::Permutor;
use List::Util qw(sum);
my $perm = new List::Permutor split('', '0123456789');
my @magic_nums;
while (my @set = $perm->next)
{
    push @magic_nums, join '', @set if
    (
       ($set[1] . $set[2] . $set[3]) %  2 == 0
    && ($set[2] . $set[3] . $set[4]) %  3 == 0
    && ($set[3] . $set[4] . $set[5]) %  5 == 0
    && ($set[4] . $set[5] . $set[6]) %  7 == 0
    && ($set[5] . $set[6] . $set[7]) % 11 == 0
    && ($set[6] . $set[7] . $set[8]) % 13 == 0
    && ($set[7] . $set[8] . $set[9]) % 17 == 0
    );
}
my %result = map { $_ => 1 } @magic_nums;
print join "\n", keys %result;
printf "\n%d\n", sum(keys %result);
