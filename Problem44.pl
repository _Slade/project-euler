#!/usr/bin/perl
use 5.018;
use warnings;
my %pen = map { $_ * (3 * $_ - 1) / 2 => $_ } (1 .. 3000);
my @pen_list = sort { $a <=> $b } keys %pen;
for my $j (@pen_list)
{
    for my $k (@pen_list)
    {
        if ($pen{$j + $k} && $pen{$j - $k})
        {
            printf "P%d and P%d: D = %d\n", $pen{$j}, $pen{$k}, abs $k - $j;
        }
    }
}
