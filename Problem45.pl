#!/usr/bin/perl
use 5.018;
use warnings;

my @tri = map { $_*(  $_+1)/2      } (1..99999);
my %pen = map { $_*(3*$_-1)/2 => 1 } (1..99999);
my %hex = map { $_*(2*$_-1)   => 1 } (1..99999);
say join ' ', grep { $pen{$_} and $hex{$_} } @tri;
