#!/usr/bin/perl
use 5.020;
use warnings;
use Memoize;

# Sum[(i = 1 -> n) f(i)]
sub S
{
    my $augend = 0;
    $augend += f($_) for (1 .. shift);
    return $augend;
}

sub f
{
    my $n = int (shift() + 0.5);
    if ($n == 1 or $n == 3)
    {
        return $n;
    }
    if ($n % 2 == 0)
    {
        return f($n / 2);
    }
    my $x = ($n - 1) / 4;
    if (is_int($x))
    {
        return 2 * f(2 * $x + 1) - f($x);
    }
    $x = ($n - 3) / 4;
    if (is_int($x))
    {
        return 3 * f(2 * $x + 1) - 2 * f($x);
    }
}

sub is_int
{
    return $_[0] == int($_[0]);
}

Memoize::memoize('f');
say S(3 ** 37);
