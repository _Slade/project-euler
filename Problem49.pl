#!/usr/bin/perl -w
use 5.018;
use strict;
use Math::Prime::Util qw<is_prime forprimes>;
use List::MoreUtils qw<uniq>;
use Benchmark qw<timethese>;
my @primes;
forprimes { push @primes, $_ if (is_prime($_+3330) && is_prime($_+6660)) } (1000,9999);
my @result;
for my $x (@primes)
{
    for my $y (@primes)
    {
        if ($x != $y && is_perm($x, $y))
        {
            push @result, $x, $y;
        }
    }
}
@result = uniq(@result);
print join "\n", @result;
my @result2 = map { join '', sort { $a <=> $b } split '', $_ } @result;
print "\n\n";
print join "\n", @result2;

sub is_perm
{
    my ($x, $y) = @_;
    my @prime1 = sort { $a <=> $b } split '', $x;
    my @prime2 = sort { $a <=> $b } split '', $y;
    for (0..3)
    {
        return 0 if ($prime1[$_] != $prime2[$_]);
    }
    1;
}
