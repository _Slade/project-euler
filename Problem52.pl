#!/usr/bin/perl
use 5.018;
use warnings;

sub is_anagram
{
    my $n = shift;
    my @multiples;
    for (2 .. 6)
    {
        my $temp = $n * $_;
        return 0 if (length $n != length $temp);
        return 0 if ((join '', (sort (split '', $temp))) != $n);
    }
    1;
}

for (my $n = 1; $n < 3074457345618258602; $n++)
{
    say $n and last if (is_anagram($n));
}
