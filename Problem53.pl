#!/usr/bin/perl
use 5.018;
use warnings;
# use Benchmark 'timethese';

my $total;
$total += C($_) for (1 .. 100);
print "$total\n";

#                   n!
# Let C(n, r) = ----------; r, n ∈ ℤ; 0 < r < n
#                r!(n-r)!
# Let n! = n*(n-1)*...*2*1; 0! = 1

sub C
{
  # Let k = |{n, r : C(n, r) > 1,000,000}|
    my $k = 0;
    my $n = shift;
    for my $r (1 .. $n)
    {
        if ((fac($n) / (fac($r) * (fac($n-$r)))) > 1_000_000)
        {
            print "n = $n, r = $r\n";
            ++$k;
        }
    }
    return $k;
}

sub fac
{
    my ($f, $n) = (1, shift);
    $f *= $_ for (2 .. $n);
    $f;
}

# Pretty, but a stack smasher. Iterative is better.
# sub fac
# {
#     no warnings 'recursion';
#     my $n = shift;
#     my $f = ($_[0]) ? (shift) : (1);
#     if ($n <= 1) { return $f };
#     return fac($n - 1, $f * $n);
# }
