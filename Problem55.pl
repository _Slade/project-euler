#!/usr/bin/perl -W
use 5.018;
use strict;
use bigint;

sub is_palindrome
{
    my $n = shift;
    my $num = $n;
    my $rev = 0;
    while ($num > 0)
    {
        my $dig = $num % 10;
        $rev = $rev * 10 + $dig;
        $num = $num / 10;
    }
    return ($n == $rev);
}

sub is_lychrel
{
    my $n = shift;
    for (1..50)
    {
        $n += (reverse $n->Math::BigInt::bstr());
        return 0 if is_palindrome($n); 
    }
    return 1;
}

my @lychrel_numbers = grep { is_lychrel($_) } (1..10_000);
print scalar @lychrel_numbers, "\n";
