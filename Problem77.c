#include <stdio.h>
#include <math.h>

double a (double n)
{
    return exp(2.0 * M_PI * sqrt(n / log(n)) / sqrt(3));
}

int main(int argc, char **argv)
{
    int i;
    for (i = 2; ; ++i)
    {
        double digits = a((double) i);
        if (digits > 5000)
        {
            printf(
                "The first number that can be partitioned "
                "into >=5,000 prime parts is %d with %f parts",
                i,
                digits
            );
            break;
        }
    }
    return 0;
}
