#include <stdio.h>
#include <stdlib.h>

int main(void)
{
    int a, b, c;
    for (a = 1; a < 1000; ++a)
    {
        for (b = 1; b < 1000; ++b)
        {
            c = 1000 - a - b;
            if (c * c == (a * a) + (b * b))
            {
                printf("a = %d, b = %d, c = %d\n", a, b, c);
                return EXIT_SUCCESS;
            }
        }
    }
    return EXIT_SUCCESS;
}

