public class Problem9
{
    public static void main (String[] args)
    {
        for (int a = 1; a < 1000; a++)
        {
            for (int b = 1; b < 1000; b++)
            {
                int c = 1000 - a - b;
                if (c*c == a*a+b*b)
                    System.out.printf("%d %d %d\n", a, b, c);
            }
        }
    }
}
