#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <stdint.h>
extern int sum_square_digits(int n);

int main(int argc, char **argv)
{
    /*
    int i;
    for (i = 0; i < 9999999; ++i)
    {
        length = num_length(i);
    }
    */
    printf("%d\n", sum_square_digits(123456789));
    return 0;
}

int sum_square_digits (int n)
{
    int digits[15];
    int length = 1;
    int augend = 0;
    int count  = 0;
    int i;

    if (n != 0)
        length = floor(log10(abs(n))) + 1;

    for (i = length; i > 0; --i)
    {
        digits[i] = (n /= 10);
        ++count;
    }

    for (i = 0; i < count; ++i)
    {
        augend += digits[i] * digits[i];
    }
    return augend;
}
