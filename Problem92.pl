#!/usr/bin/perl
use warnings;
use strict;
use integer;
use feature qw(say);
use List::AllUtils qw(sum);

my ($count, $iter);
for my $n (1 .. 1e7)
{
    my $split = $n;
    while ($split != 89 && $split != 1)
    {
        $split = sum map { $_ * $_ } split '', $split;
    }
    ++$count if ($split == 89);
    ++$iter;
    if ($iter % 10000 == 0)
    {
        say "$iter iterations in so far.";
    }
}
say $count;
