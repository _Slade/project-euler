#!/usr/bin/perl
use 5.018;
use warnings;
use bigint;
use constant MOD => 1e10;
my $num = 28_433 * 2->bmodpow(7_830_457, MOD) + 1;
say $num % MOD;
